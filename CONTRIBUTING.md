# Merge Requests Guidelines

## 1. Descriptions
 * We can think of templates. We can decide on the content of this template

## 2. Size of MR
 * Smaller MRs. Solving a particular problem/feature
 * Lets avoid scope creep

## 3. Reviews
 * Review comments should be resolved by the reviewer
 * Should all the comments be resolved?  Yes, If it’s just a comment any one can resolve it
 * An MR can only be merged with one positive review. But an MR should have more than one reviewer when possible.Number of reviewers >= 1?

## 4. Commits
 * Strive for one commit per MR unless it make sense to add more commits
 * Commits should be atomic and logical to enable post corrective or selective actions like bisect, revert, cherry-pick ...
 * Squashing can be used to bundle multiple commits to achieve a logical patch set

## 5. Coverage
 * New line of code should have test coverage. It should not lower the coverage numbers

## 6. Documentation
 * Should have minimal documentation(outside the code). Mandatory for the new features
 * Traceability from kanban to MRs and vice versa
     * Put the JIRA ticket in the MR Title\
          **Example**
          ```
          Title of the MR [VROOM-XXX]
          ```
     * We should not track the board from the MR
