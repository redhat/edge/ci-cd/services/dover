FROM registry.ci.openshift.org/openshift/release:golang-1.16 as builder

WORKDIR /workspace

COPY ./ /workspace

RUN go mod download && go mod vendor

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o dover main.go

FROM registry.access.redhat.com/ubi8/ubi:8.4

COPY --from=builder /workspace/dover /usr/local/bin/dover

EXPOSE 8080

WORKDIR /workspace/dover

USER 65534:65534

CMD ["/usr/local/bin/dover"]
