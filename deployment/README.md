# Deployment

## Installation

- Install the required modules listed in the `deployment/requirements.txt`
```
pip install -r deployment/requirements.txt
```
- Download and install [OpenShift CLI](https://docs.openshift.com/container-platform/4.8/cli_reference/openshift_cli/getting-started-cli.html)

## Prerequisite

- Login to the cluster
  - Using the `oc login` command retrieved from the openshift cluster 
## Auto deployment

Auto deployment automates the process of manually building and deploying the image to the cluster. The defined rules will take care of triggering the deployment process for every merge event on the **main** branch. This is as well done by integrating with [GitLab Webhook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html)

### To push the deployment script

**Prerequisite**

Populate the request values to `deployment/defaults/main.yml`

```
project_id: gitlab project ID
group: gitlab group
branch: gitlab branch

gitlab_token: gitlab API token
gitlab_key: the key that will identify the webhook (arbitrary)
```

Then, provide the following values in either `/deployment/vars/stage.yml` or `/deployment/vars/prod.yml` based on the environment.

```
ocp_ns: #namespace into which k8s resources will be deployed
validate_certs: #value of yes/no
ocp_host: #url of the OpenShift host
ocp_key: #API access token to the openshift host
```

Navigate to the project root directory:

#### Start staging environment

```
make stage/up
```

#### Start production environment

```
make prod/up
```

#### Developer mode

```make
make image/run/dev GITLAB_TOKEN=<gitlab API token>
```

### Openshift

```yaml
---
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: dover-secret
  namespace: automotive-ci
data:
  GITLAB_TOKEN: <base64 encoded gitlab access token>

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dover
  namespace: automotive-ci
spec:
  replicas: 1
  selector:
    matchLabels:
      run: dover
  template:
    metadata:
      labels:
        run: dover
    spec:
      containers:
        - image: quay.io/hstefans/dover
          name: dover
          hostname: dover
          ports:
            - containerPort: 8080
          imagePullPolicy: Always
          securityContext:
            type: MustRunAsNonRoot
          envFrom:
            - secretRef:
                name: dover-secret
          volumeMounts:
            - mountPath: "/package_list"
              name: dover-volume
      volumes:
        - name: dover-volume
          emptyDir: { }

---
apiVersion: v1
kind: Service
metadata:
  name: dover
spec:
  type: ClusterIP
  selector:
    run: dover
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 8080
```
