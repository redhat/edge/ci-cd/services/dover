module gitlab.com/ateam/dover

go 1.16

require (
	github.com/apsdehal/go-logger v0.0.0-20190515212710-b0d6ccfee0e6
	github.com/google/go-github v17.0.0+incompatible
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/hprose/hprose-go v0.0.0-20161031134501-83de97da5004
	github.com/sirupsen/logrus v1.8.1
	github.com/xanzy/go-gitlab v0.6.0
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
	golang.org/x/oauth2 v0.0.0-20210819190943-2bc19b11175f
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
)
