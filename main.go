package main

import (
	logger "github.com/sirupsen/logrus"
	"gitlab.com/ateam/dover/pkg/router"
)

func main() {
	if err := router.StartServer(); err != nil {
		logger.Error("error", err)
	}
}
