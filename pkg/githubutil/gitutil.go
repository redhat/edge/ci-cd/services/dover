package gitutil

import (
	"context"
	"errors"
	"fmt"

	logger "github.com/sirupsen/logrus"

	"io/ioutil"
	"os"
	"strings"
	"time"

	"gitlab.com/ateam/dover/constants"

	"github.com/google/go-github/github"
	"golang.org/x/oauth2"
)

// GitClient is the interface for github functions.
type GitClient struct {
	Ctx      context.Context
	Client   *github.Client
	Upstream string
}

// NewGitClient returns a new github client using the GITHUB_TOKEN environment variable.
func NewGitClient() (*GitClient, error) {
	ctx := context.Background()
	GithubToken := os.Getenv(constants.GithubTokenEnvKey)

	if GithubToken == "" {
		return nil, errors.New("required Env Var GITHUB_TOKEN not found")
	}

	repo := constants.GitHubProdOrg
	if os.Getenv(constants.DoverDryRunKey) == "true" {
		repo = constants.GitHubDevOrg
	}

	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: GithubToken},
	)
	tc := oauth2.NewClient(ctx, ts)

	return &GitClient{
		Ctx:      ctx,
		Client:   github.NewClient(tc),
		Upstream: repo,
	}, nil
}

func (g *GitClient) getRef(newBranchName string) (*github.Reference, error) {
	if newBranchName == "" || newBranchName == g.Upstream {
		return nil, errors.New("branch conflict or undefined")
	}

	if ref, _, err := g.Client.Git.GetRef(g.Ctx, g.Upstream, constants.GithubRepo, constants.GithubRefPrefix+newBranchName); err == nil {
		return ref, nil
	}

	var baseRef *github.Reference

	baseRef, _, err := g.Client.Git.GetRef(g.Ctx, g.Upstream, constants.GithubRepo, constants.GithubRefPrefix+constants.GithubProdBranch)
	if err != nil {
		return nil, err
	}

	newRef := &github.Reference{
		Ref:    github.String(constants.GithubRefPrefix + newBranchName),
		Object: &github.GitObject{SHA: baseRef.Object.SHA}}

	ref, _, err := g.Client.Git.CreateRef(g.Ctx, g.Upstream, constants.GithubRepo, newRef)

	return ref, err
}

func (g *GitClient) getTree(ref *github.Reference, sourceFiles []*string) (*github.Tree, error) {
	var entries []github.TreeEntry

	var tree *github.Tree

	for _, fileValue := range sourceFiles {
		file, content, err := getFileContent(*fileValue)
		if err != nil {
			return nil, err
		}

		entries = append(entries, github.TreeEntry{
			Path:    github.String(file),
			Type:    github.String("blob"),
			Content: github.String(string(content)),
			Mode:    github.String(constants.GithubTreeEntryMode),
		})
	}

	tree, _, err :=
		g.Client.Git.CreateTree(g.Ctx, g.Upstream, constants.GithubRepo, *ref.Object.SHA, entries)

	return tree, err
}

func getFileContent(file string) (targetName string, b []byte, err error) {
	var localFile string

	files := strings.Split(file, ":")

	switch {
	case len(files) < 1:
		return "", nil, errors.New("no files found")
	case len(files) == 1:
		localFile = files[0]
		targetName = files[0]
	default:
		localFile = files[0]
		targetName = files[1]
	}

	b, err = ioutil.ReadFile(localFile)

	return targetName, b, err
}

func (g *GitClient) pushCommit(ref *github.Reference, tree *github.Tree, commitMessage string) error {
	parent, _, err := g.Client.Repositories.GetCommit(g.Ctx, g.Upstream,
		constants.GithubRepo, *ref.Object.SHA)
	if err != nil {
		return err
	}

	parent.Commit.SHA = parent.SHA
	date := time.Now()

	author := &github.CommitAuthor{
		Date:  &date,
		Name:  github.String(constants.RomeBotName),
		Email: github.String(constants.RomeBotEmail),
	}

	commit := &github.Commit{
		Author:  author,
		Message: github.String(commitMessage),
		Tree:    tree,
		Parents: []github.Commit{*parent.Commit},
	}

	newCommit, _, err := g.Client.Git.CreateCommit(g.Ctx, g.Upstream, constants.GithubRepo, commit)
	if err != nil {
		logger.Errorf("%v", err)
		return err
	}

	ref.Object.SHA = newCommit.SHA

	_, _, err = g.Client.Git.UpdateRef(g.Ctx, g.Upstream, constants.GithubRepo, ref, true)

	return err
}

func (g *GitClient) createPR(commitMessage, branchName string) (*github.PullRequest, error) {
	body := fmt.Sprintf("## Rome-bot Automated Pull Request \n ### Change: \n %s \n", commitMessage)
	PR := &github.NewPullRequest{
		Title:               github.String(fmt.Sprintf("[Rome-Automation]: %s", commitMessage)),
		Head:                github.String(g.Upstream + ":" + branchName),
		Base:                github.String(constants.GithubProdBranch),
		Body:                github.String(body),
		MaintainerCanModify: github.Bool(true),
	}

	pr, _, err := g.Client.PullRequests.Create(g.Ctx, g.Upstream, constants.GithubRepo, PR)

	return pr, err
}

// SubmitPR pushes the commit to github.
func (g *GitClient) SubmitPR(branchName, commitMessage string, sourceFiles []*string) error {
	glogger := logger.WithFields(logger.Fields{
		"component":      "gitutil",
		"commit_message": commitMessage,
		"branch":         branchName,
	})

	if os.Getenv(constants.DoverDryRunKey) == "true" {
		glogger.Info("DOVER_DRY_RUN set, no PR was created")
		return nil
	}

	glogger.Debug("creating new branch reference")

	ref, err := g.getRef(branchName)
	if err != nil {
		glogger.Error(err)
		return fmt.Errorf("unable to get/create the commit reference: %w", err)
	}

	if ref == nil {
		glogger.Error(err)
		return fmt.Errorf("no error was returned but the reference is nil")
	}

	glogger.Debug("retrieving tree")

	tree, err := g.getTree(ref, sourceFiles)
	if err != nil {
		glogger.Error(err)
		return fmt.Errorf("unable to create the tree based on the provided files: %w", err)
	}

	glogger.Debug("pushing commit")

	if err := g.pushCommit(ref, tree, commitMessage); err != nil {
		return fmt.Errorf("unable to create the commit: %w", err)
	}

	if commitMessage == "test" {
		return nil
	}
	glogger.Debug("creating and pushing new pull request")

	pr, err := g.createPR(commitMessage, branchName)
	if err != nil {
		glogger.Error(err)
		return fmt.Errorf("error while creating the pull request: %w", err)
	}

	if pr == nil {
		glogger.Error(err)
		return fmt.Errorf("new PR returned nil")
	}

	glogger.WithFields(logger.Fields{
		"pr_url":       pr.HTMLURL,
		"pr_createdAt": pr.CreatedAt,
		"pr_title":     pr.Title}).Info("Pull Request created")

	return nil
}
