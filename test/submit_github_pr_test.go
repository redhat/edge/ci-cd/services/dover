package test

import (
	"bytes"
	"log"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"gitlab.com/ateam/dover/pkg/router"
)

func TestSubmitPackageListHandler(t *testing.T) {
	os.Setenv("GITHUB_TOKEN", "Test")
	os.Setenv("DOVER_DRY_RUN", "true")

	data := `{"cs8": { "common": ["a", "b", "c"], "arch": {"x86_64": ["d", "e", "f"], "aarch64": ["g", "h", "i"]}}}`
	fileName := "cs8-image-manifest.json"
	params := map[string]string{"branch": "dover-test-branch", "commitMessage": "test", "data": data, "fileName": fileName}

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	for key, val := range params {
		_ = writer.WriteField(key, val)
	}

	err := writer.Close()
	if err != nil {
		t.Fatal(err)
	}

	log.Print(body)

	req, err := http.NewRequest("POST", "/submit/package_list", body)
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Set("boundary", "-------------------------------dover")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(router.SubmitPackageListHandlerGithub)

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("unexpected handler return code got %v expected %v", rr.Code, http.StatusOK)
	}
}
