package test

import (
	"bytes"
	"log"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/ateam/dover/constants"
	"gitlab.com/ateam/dover/pkg/gitlabutil"
	"gitlab.com/ateam/dover/pkg/router"
)

const pID = "28719912"

func CleanUp(gitClient *gitlabutil.GitClientAttr, repoAttr *gitlabutil.RepoAttr, branchAttr *gitlabutil.BranchAttr) {
	_, _ = gitClient.Client.DeleteBranch(&gitlabutil.RepoAttr{ProjectID: pID}, branchAttr)
}

// SubmitPackageListHandlerGitlabMock provides an exact mock for SubmitPackageListHandlerGitlab
// It sets the token , URL and projetcId so that the test can target a new repo to test the
// functionality from end to end
func SubmitPackageListHandlerGitlabMock(w http.ResponseWriter, r *http.Request) {
	tempFile, err := router.ReadDataFromEndPoint(w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var gitlab gitlabutil.GitForge = &gitlabutil.Gitlab{Client: &gitlab.Client{}}
	gitClient := gitlabutil.NewGitClientAttr(&gitlab)

	if gitClient == nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	envVar := gitlabutil.EnvVars{}
	// Read the environment vars before creating the client
	if envVar.GetEnv() != nil {
		return
	}

	gitClient.Client.CreateClient(&envVar)

	if gitClient.SetBaseURLWrapper("https://gitlab.com/api/v4/") != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	branchAttr := gitlabutil.BranchAttr{SourceBranch: r.Form.Get("branch"), TargetBranch: constants.GitlabProdBranch}
	br, err := gitClient.CreateBranchWrapper(&gitlabutil.RepoAttr{ProjectID: pID}, &branchAttr)

	if err != nil || br == nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	commitMessage := r.Form.Get("commitMessage")
	commit, err := gitClient.CreateCommitWrapper(&gitlabutil.RepoAttr{ProjectID: envVar.GitlabProjectID}, &gitlabutil.
		CommitAttr{AuthorName: constants.RomeBotName, AuthorEmail: "automotive-a-team@redhat.com", CommitMsg: commitMessage,
		SourceFile: tempFile.Name()}, &branchAttr)
	if err != nil || commit == nil {
		_, _ = gitClient.Client.DeleteBranch(&gitlabutil.RepoAttr{ProjectID: pID}, &branchAttr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	mr, err := gitClient.CreateMRWrapper(&gitlabutil.RepoAttr{ProjectID: pID}, &gitlabutil.MergeAttr{Title: "Rome-bot Automated Merge Request Change",
		Description: commitMessage}, &branchAttr)

	if err != nil || mr == nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	_ = os.Remove(constants.PackageListFileDirectory + "/" + tempFile.Name())

	w.WriteHeader(http.StatusOK)
	CleanUp(gitClient, &gitlabutil.RepoAttr{ProjectID: envVar.GitlabProjectID}, &branchAttr)
}

// TODO: This test depends on a repo on the internet, which is an external dependency
// and not in control of the test environment which is not best practice.
// Therefore it needs an improvement to have a local environment set for the functional test.
// There are some solutions to it and it will be explored later
func TestSubmitgitlabMR(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", "x_tuWAYA6Ph4xLourHbt")
	os.Setenv("PROJECT_ID", pID)

	data := `{"cs8": { "common": ["a", "b", "c"], "arch": {"x86_64": ["d", "e", "f"], "aarch64": ["g", "h", "i"]}}}`
	fileName := "cs8-image-manifest.json"
	params := map[string]string{"branch": "dover-test-branch", "commitMessage": "test", "data": data, "fileName": fileName}

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	for key, val := range params {
		_ = writer.WriteField(key, val)
	}

	err := writer.Close()
	if err != nil {
		t.Fatal(err)
	}

	log.Print(body)

	req, err := http.NewRequest("POST", "/submit/package_list", body)
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Set("boundary", "-------------------------------dover")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(SubmitPackageListHandlerGitlabMock)

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("unexpected handler return code got %v expected %v", rr.Code, http.StatusOK)
	}
}
